sap.ui.define([
	'icms/em/controller/BaseController',
	'sap/ui/core/mvc/Controller',
	'sap/ui/model/json/JSONModel',
	'sap/ui/core/UIComponent',
	'sap/m/MessageBox',
	'icms/em/model/Callmodel'
], function (BaseController, Controller, JSONModel, UIComponent, MessageBox, CallModel) {
	"use strict";

	return BaseController.extend("icms.em.controller.Login", {
		onInit: function (evt) {
			BaseController.prototype.onInit.apply(this, arguments);
			this.getRouter().getRoute("login").attachPatternMatched(this._loginMatched, this);
		},

		_loginMatched: function () {

		},

		loginControl: function (evt) {
			this.openDialogBusy(this._getLocalText("loadingData"));
			var usernameInput = $("[id*='User']:input");
			var passwordInput = $("[id*='Password']:input");
			var user = window.btoa(usernameInput[0].value);
			var password = window.btoa(passwordInput[0].value);
			this.getUserInfo(user, password);
		},
		getUserInfo: function (user, password) {
			var fSuccess = function (result) {
				var userSession = {
					cid: result.cid,
					user: result.user
				}
				this._setSessionStorageItem('loginInfo', userSession);
				var oRouter = UIComponent.getRouterFor(this);
				oRouter.navTo("launchpad");
				this.closeDialogBusy();
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("loginErrorMessage");
				MessageBox.error(this._getLocalText(msgError));
				this._removeSessionStorageItem('loginInfo');
				this.closeDialogBusy();
			}.bind(this);
			CallModel.getUserList(user, password).then(fSuccess, fError);
		}
	});
});