sap.ui.define([
	'icms/em/controller/BaseController',
	'jquery.sap.global',
	'sap/ui/core/mvc/Controller',
	'sap/ui/model/json/JSONModel',
	'sap/ui/core/UIComponent',
	'icms/em/model/TileModel',
	'icms/em/model/Formatter'
], function (BaseController, jQuery, Controller, JSONModel, UIComponent, tileModel, Formatter) {
	"use strict";

	return BaseController.extend("icms.em.controller.Launchpad", {
		onInit: function (evt) {
			BaseController.prototype.onInit.apply(this, arguments);
			this.getRouter().getRoute("launchpad").attachPatternMatched(this._launchpadMatched, this);
			this.launchpadModel = new JSONModel();
			this.getView().setModel(this.launchpadModel, "launchpadModel");
		},

		_launchpadMatched: function () {
			BaseController.prototype._userRouteMatched.apply(this, arguments);
			this._setPage();
			this.launchpadModel.setProperty("/todayDate", Formatter.formatDateLaunchpad(new Date()));
		},

		_setPage: function () {
			this.openDialogBusy(this._getLocalText("loadingData"));
			this._getTiles();
		},

		_getTiles: function () {
			var tiles = tileModel.getTiles();
			tiles.forEach(function (n) {
				n.title = this._getLocalText(n.title)
			}.bind(this));
			this.launchpadModel.setData(tiles);
			this.getView().rerender();
			this.closeDialogBusy();
		},

		onTilePress: function (evt) {
			var selectedTile = evt.getSource().getBindingContext("launchpadModel").getObject();
			var oRouter = UIComponent.getRouterFor(this);
			oRouter.navTo(selectedTile.url);
		},
		
		onLogOutPress: function () {
			this._removeSessionStorageItem('loginInfo');
			var oRouter = UIComponent.getRouterFor(this);
			oRouter.navTo("login");
		}
	});
});