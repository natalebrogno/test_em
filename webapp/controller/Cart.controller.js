sap.ui.define([
	'icms/em/controller/BaseController',
	'sap/ui/model/json/JSONModel',
	'sap/ui/core/UIComponent',
	'icms/em/model/Formatter',
	'sap/m/MessageBox',
], function (BaseController, JSONModel, UIComponent, Formatter, MessageBox) {
	"use strict";

	return BaseController.extend("icms.em.controller.Cart", {
		formatter: Formatter,
		onInit: function (evt) {
			BaseController.prototype.onInit.apply(this, arguments);
			this.getRouter().getRoute("cart").attachPatternMatched(this._cartMatched, this);
			this.filterDataModel = new JSONModel();
			this.getView().setModel(this.filterDataModel, "filterDataModel");
			this.cartModel = new JSONModel();
			this.getView().setModel(this.cartModel, "cartModel");
		},

		_cartMatched: function () {
			BaseController.prototype._userRouteMatched.apply(this, arguments);
			this.setfilterDataModel();
			this.setCartData();
		},

		setfilterDataModel: function () {
			var filterData = this._getSessionStorageItem('filterData');
			this.filterDataModel.setData(filterData);
			this.filterDataModel.setProperty("/protocolNumber", "");
			this.filterDataModel.setProperty("/docMM", "1000");
			this.filterDataModel.setProperty("/enabledInput", false);
		},
		
		setCartData: function () {
			var cartData = this._getSessionStorageItem('cartData');
			this.cartModel.setData(cartData);
		},

		onWriteProtocolNumber: function (evt) {
			var input = evt.getSource();
			var value = input.getValue();
			if (isNaN(value)) {
				value = value.substring(0, value.length - 1);
				input.setValue(value);
			}
			this.userModel.setProperty("/protocolNumber", value);
		},

		removeItemsSelect: function (evt) {
			var src = evt.getSource();
			var pathSelect = src.getParent().getBindingContext("cartModel").getPath();
			var splitSelect = pathSelect.split("/", );
			var indexSelect = parseInt(splitSelect[1]);
			var newCartData = this.cartModel.getData();
			newCartData.splice(indexSelect, 1);
			if (newCartData.length !== 0) {
				this._setSessionStorageItem('cartData', newCartData);
				this.setCartData();
			} else {
				this._setSessionStorageItem('cartData', newCartData);
				this.setCartData();
				this.navBack();
			}
		},

		onPressRecDocument: function (evt) {
			MessageBox.confirm(this._getLocalText("confirmRecDocument"), {
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				onClose: _.bind(this._pressRecDocument, this)
			});
		},

		_pressRecDocument: function (evt) {
			if (evt === MessageBox.Action.NO) {
				return;
			}
			var filterModel = this.filterDataModel.getData();
			var cartModel = this.cartModel.getData();
			var userModel = this.userModel.getData();
			this.createDocPosMM(cartModel);
			var documentDataStorage = this._getLocalStorageItem('documentsData');
			if (!documentDataStorage) {
				documentDataStorage = [];
				var newDocument = {
					"warehouse": filterModel.warehouse,
					"warehousesDesc": filterModel.warehousesDesc,
					"bolla": filterModel.bolla,
					"dateDoc": filterModel.dateDoc,
					"dateRec": filterModel.dateRec,
					"dateCreateDoc": new Date(),
					"protocolNumber": filterModel.protocolNumber,
					"docMM": filterModel.docMM,
					"supplier": filterModel.supplier,
					"suppliersDesc": filterModel.suppliersDesc,
					"listCartModel": cartModel,
					"userLogin": userModel.user
				}
				documentDataStorage.push(newDocument);
				this._setLocalStorageItem('documentsData', documentDataStorage);
				MessageBox.success(this._getLocalText("documentNumber") + newDocument.docMM + " " + this._getLocalText("successRecDocument"), {
					onClose: _.bind(this._onCloseMessagge, this)
				});
			} else {
				var ultimateDoc = documentDataStorage[documentDataStorage.length - 1];
				var newDocument = {
					"warehouse": filterModel.warehouse,
					"warehousesDesc": filterModel.warehousesDesc,
					"bolla": filterModel.bolla,
					"dateDoc": filterModel.dateDoc,
					"dateRec": filterModel.dateRec,
					"dateCreateDoc": new Date(),
					"protocolNumber": filterModel.protocolNumber,
					"docMM": (parseInt(ultimateDoc.docMM) + 1).toString(),
					"supplier": filterModel.supplier,
					"suppliersDesc": filterModel.suppliersDesc,
					"listCartModel": cartModel,
					"userLogin": userModel.user
				}
				documentDataStorage.push(newDocument);
				this._setLocalStorageItem('documentsData', documentDataStorage);
				MessageBox.success(this._getLocalText("documentNumber") + newDocument.docMM + " " + this._getLocalText("successRecDocument"), {
					onClose: _.bind(this._onCloseMessagge, this)
				});
			}
		},

		_onCloseMessagge: function (evt) {
			this._removeSessionStorageItem('filterData');
			this._removeSessionStorageItem('cartData');
			this.closeDialogBusy();
			history.back(-1);
		},

		createDocPosMM: function (oModel) {
			var ipos = 1;
			oModel.forEach(function (n) {
				var row = n;
				row.posDocMM = "0000" + (ipos);
				ipos++;
			});
			return oModel;
		},

		navBack: function (evt) {
			this.closeDialogBusy();
			history.back(-1);
		}
	});
});