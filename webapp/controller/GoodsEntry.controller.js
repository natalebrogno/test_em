sap.ui.define([
	'icms/em/controller/BaseController',
	'sap/ui/model/json/JSONModel',
	'sap/ui/core/UIComponent',
	'icms/em/model/Formatter',
	'sap/m/MessageBox',
	'icms/em/model/Callmodel'
], function (BaseController, JSONModel, UIComponent, Formatter, MessageBox, CallModel) {
	"use strict";

	return BaseController.extend("icms.em.controller.Goodsentry", {
		formatter: Formatter,
		onInit: function (evt) {
			BaseController.prototype.onInit.apply(this, arguments);
			this.getRouter().getRoute("goodsentry").attachPatternMatched(this._goodsentryMatched, this);
			this.filterModel = new JSONModel();
			this.getView().setModel(this.filterModel, "filterModel");
			this.visibleModel = new JSONModel();
			this.getView().setModel(this.visibleModel, "visibleModel");
			this.listModel = new JSONModel();
			this.getView().setModel(this.listModel, "listModel");
		},

		_goodsentryMatched: function () {
			BaseController.prototype._userRouteMatched.apply(this, arguments);
			this.sessionModel = this._getSessionStorageItem('filterData');
			this.cartSessionModel = this._getSessionStorageItem('cartData') ? this._getSessionStorageItem('cartData') : [];
			this.visibleModel.setProperty("/expanded", false);
			this.visibleModel.setProperty("/expanndedHeader", true);
			this.visibleModel.setProperty("/visible", false);
			this.visibleModel.setProperty("/visibleTable", false);
			this.visibleModel.setProperty("/enabled", true);
			this.visibleModel.setProperty("/state", "None");
			this.visibleModel.setProperty("/visibleCartButton", false);
			this.visibleModel.setProperty("/visibleEmptyCartButton", false);
			this.visibleModel.setProperty("/onSwitchPos", true);
			this.visibleModel.setProperty("/onSwitchMat", true);
			this.visibleModel.setProperty("/cartSession", true);
			this.setHeaderSubFilterModel();
			this.setListModel();
			this.setHeaderFilterModel();
		},

		setHeaderFilterModel: function () {
			this.filterModel.setProperty("/dateDoc", null);
			this.filterModel.setProperty("/dateRec", null);
			this.filterModel.setProperty("/bolla", "");
			this.filterModel.setProperty("/warehouse", "");
			this.filterModel.setProperty("/warehouses", []);
			this.filterModel.setProperty("/supplier", "");
			this.filterModel.setProperty("/suppliers", []);
			this.getWarehouses();
		},

		setHeaderSubFilterModel: function () {
			// Le posizioni sono presenti dentro l'array orders
			this.filterModel.setProperty("/order", []);
			this.filterModel.setProperty("/orders", [])
			this.filterModel.setProperty("/positions", []);
			this.filterModel.setProperty("/position", []);
			this.filterModel.setProperty("/material", []);
			this.filterModel.setProperty("/materials", []);
			this.filterModel.setProperty("/dateDa", null);
			this.filterModel.setProperty("/dateA", null);
		},

		setInputState: function (evt) {
			var input = evt.getSource().getValue();
			if (input) {
				this.visibleModel.setProperty("/state", "Success");
			} else
				this.visibleModel.setProperty("/state", "None");
		},

		setListModel: function () {
			this.listModel.setProperty("/listPosition", []);
			this.listModel.setProperty("/listOrder", []);
		},

		getWarehouses: function () {
			var fSuccess = function (result) {
				this.filterModel.setProperty("/warehouses", result);
				if (this.sessionModel) {
					this.filterModel.setProperty("/dateDoc", Formatter.formatDateSession(this.sessionModel.dateDoc));
					this.filterModel.setProperty("/dateRec", Formatter.formatDateSession(this.sessionModel.dateRec));
					this.filterModel.setProperty("/bolla", this.sessionModel.bolla);
					this.filterModel.setProperty("/warehouse", this.sessionModel.warehouse);
					this.getSuppliers(this.sessionModel.warehouse);
				}
				this.getView().setBusy(false);
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);
			CallModel.getWarehouseList().then(fSuccess, fError);
		},
		onWarehouseChange: function (evt) {
			var warehouseselect = evt.getSource().getSelectedKey();
			this.getSuppliers(warehouseselect);
		},
		getSuppliers: function (warehouseselect) {
			var fSuccess = function (result) {
				this.filterModel.setProperty("/suppliers", result);
				if (this.sessionModel) {
					this.filterModel.setProperty("/supplier", this.sessionModel.supplier);
					this.getOrders(this.sessionModel.supplier);
				}
				this.getView().setBusy(false);
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);
			CallModel.getSupplierList(warehouseselect).then(fSuccess, fError);
		},

		onPressResetOrder: function (evt) {
			MessageBox.confirm(this._getLocalText("confirmAnnulOrder"), {
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				onClose: _.bind(this._resetOrder, this)
			});
		},

		_resetOrder: function (evt) {
			if (evt === MessageBox.Action.NO) {
				return;
			}
			this._removeSessionStorageItem('filterData');
			this._removeSessionStorageItem('cartData');
			this._goodsentryMatched();
		},

		onContinuePress: function (evt) {
			this.visibleModel.setProperty("/expanded", true);
			this.visibleModel.setProperty("/expanndedHeader", false);
			this.visibleModel.setProperty("/visible", true);
			this.visibleModel.setProperty("/enabled", false);
			var supplierSelect = this.filterModel.getProperty("/supplier");
			this.getOrders(supplierSelect);
		},

		getOrders: function (supplierSelect) {
			var fSuccess = function (result) {
				var ordersToShow = _.uniqBy(result, "ordernuber");
				this.filterModel.setProperty("/orders", ordersToShow);
				if (this.sessionModel) {
					this.filterModel.setProperty("/order", this.sessionModel.order);
					this.getPosition(this.sessionModel.order);
					this.getMaterials(this.sessionModel.supplier);
				} else {
					this.getMaterials(supplierSelect);
				}
				this.getView().setBusy(false);
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);
			CallModel.getOrdersList(supplierSelect).then(fSuccess, fError);
		},

		onChangeOrders: function (evt) {
			var orderSelect = evt.getSource().getSelectedKeys();
			this.getPosition(orderSelect);
		},

		getPosition: function (orderSelect) {
			var fSuccess = function (result) {
				var positionToShow = _.uniqBy(result, "position");
				this.filterModel.setProperty("/positions", positionToShow);
				if (this.sessionModel) {
					this.filterModel.setProperty("/position", this.sessionModel.position);
				}
				this.getView().setBusy(false);
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);
			CallModel.getPositionList(orderSelect).then(fSuccess, fError);
		},

		getMaterials: function (supplierSelect) {
			var fSuccess = function (result) {
				this.filterModel.setProperty("/materials", result);
				if (this.sessionModel) {
					this.filterModel.setProperty("/material", this.sessionModel.material);
					this.filterModel.setProperty("/dateDa", Formatter.formatDateSession(this.sessionModel.dateDa));
					this.filterModel.setProperty("/dateA", Formatter.formatDateSession(this.sessionModel.dateA));
					this.visibleModel.setProperty("/expanded", false);
					this.visibleModel.setProperty("/expanndedHeader", false);
					if (this.cartSessionModel) {
						this.visibleModel.setProperty("/visibleCartButton", true);
						this.visibleModel.setProperty("/visibleEmptyCartButton", true);
						this.setSearch();
					}
				}
				this.getView().setBusy(false);
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);
			CallModel.getMaterialList(supplierSelect).then(fSuccess, fError);
		},

		setList: function (result) {
			var swichMatMode = this.visibleModel.getProperty("/onSwitchMat");
			var listFitlerOrder = [];
			var arrayOrders = this.filterModel.getData().orders;
			arrayOrders.forEach(function (i) {
				result.forEach(function (j) {
					if (i.ordernuber === j.order) {
						listFitlerOrder.push(j);
					}
				});
			});
			var ordersListToShow = _.uniqBy(listFitlerOrder, "order");
			this.listModel.setProperty("/listOrder", ordersListToShow);
			if (swichMatMode) {
				var orderListForMaterial = _.sortBy(listFitlerOrder, ['material']);
				this.listModel.setProperty("/listPosition", orderListForMaterial);
				this.allPosition = orderListForMaterial;
			} else {
				var orderListForPosition = _.sortBy(listFitlerOrder, ['position']);
				this.listModel.setProperty("/listPosition", orderListForPosition);
				this.allPosition = orderListForPosition;
			}
		},

		getList: function (SearchFilter) {
			var fSuccess = function (result) {
				if (this.sessionModel || this.cartSessionModel) {
					if (this.cartSessionModel) {
						for (var i = 0; i < this.cartSessionModel.length; i++) {
							for (var j = 0; j < result.length; j++) {
								if (this.cartSessionModel[i].order === result[j].order && this.cartSessionModel[i].position == result[j].position) {
									result[j].selected = this.cartSessionModel[i].selected;
									result[j].qbolla = this.cartSessionModel[i].qbolla;
								}
							}
						}
						this.setList(result);
					}
					////////////////////////////////////////////////////
					this.visibleModel.setProperty("/state", "Success");
					this.visibleModel.setProperty("/enabled", false);
					this.visibleModel.setProperty("/visibleTable", true);
					this.visibleModel.setProperty("/visible", true);
					this.visibleModel.setProperty("/onSwitchPos", true);
					this.visibleModel.setProperty("/onSwitchMat", true);
					this.visibleCartButton();
				} else {
					this.setList(result);
				}
				this.getView().setBusy(false);
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);
			CallModel.getListData(SearchFilter).then(fSuccess, fError);
		},

		navToOrderList: function (evt) {
			var navCon = this.byId("navCon");
			navCon.to(this.byId("orderList"));
		},

		navToPosList: function (evt) {
			var navCon = this.byId("navCon");
			navCon.to(this.byId("posList"));
		},

		navBackPosList: function (evt) {
			var src = evt.getSource();
			var obj = src._getBindingContext("listModel").getObject();
			var listFilter = [];
			// var listData = this.listModel.getProperty("/listPosition");
			this.allPosition.forEach(function (j) {
				if (obj.order === j.order) {
					listFilter.push(j);
				}
			})
			this.listModel.setProperty("/listPosition", listFilter);
			var navCon = this.byId("navCon");
			navCon.to(this.byId("posList"));
		},

		onChangePosSwitch: function (evt) {
			var state = evt.getSource().getState();
			if (state) {
				this.visibleModel.setProperty("/onSwitchPos", true);
			} else {
				this.visibleModel.setProperty("/onSwitchPos", false);
				this.visibleModel.setProperty("/onSwitchMat", true);
			}
		},

		onChangeMatSwitch: function (evt) {
			var state = evt.getSource().getState();
			if (state) {
				this.visibleModel.setProperty("/onSwitchMat", true);
			} else {
				this.visibleModel.setProperty("/onSwitchMat", false);
			}
		},

		onSearchPress: function (evt) {
			this.setSearch();
		},

		setSearch: function () {
			var SearchFilter = this.filterModel.getData();
			this.getList(SearchFilter);
			this.visibleModel.setProperty("/expanded", false);
			this.visibleModel.setProperty("/expanndedHeader", false);
			this.visibleModel.setProperty("/visibleTable", true);
			this.visibleModel.setProperty("/visibleCartButton", false);
			this.visibleModel.setProperty("/visibleEmptyCartButton", false);
			var swichPosMode = this.visibleModel.getProperty("/onSwitchPos");
			if (swichPosMode) {
				this.navToPosList();
			} else {
				this.navToOrderList();
			}
		},

		onResetPress: function (evt) {
			this.filterModel.setProperty("/order", []);
			this.filterModel.setProperty("/position", []);
			this.filterModel.setProperty("/material", []);
			this.filterModel.setProperty("/dateDa", null);
			this.filterModel.setProperty("/dateA", null);
			this.visibleModel.setProperty("/onSwitchPos", true);
			this.visibleModel.setProperty("/onSwitchMat", true);
		},

		onItemSelect: function (evt) {
			var oListModel = this.listModel.getProperty("/listPosition");
			var isSelected = evt.getParameter("listItem").getSelected();
			var idSelect = evt.getParameter("listItem").getId();
			switch (isSelected) {
				case true:
					var posSel = _.filter(oListModel, {
						selected: true
					});
					posSel.forEach(function (n) {
						n.stateInputBolla = this.checkItemsSelect(n);
					}.bind(this));
					this.visibleCartButton();
					this.listModel.refresh();
					setTimeout(function () {
						var indexSplit = idSelect.split("tablePos-");
						var idRowSelect = parseInt(indexSplit[1]);
						var inputBolla = $("[id*='tablePos-" + idRowSelect + "-inner']:input");
						inputBolla.focus();
					}, 300);
					break;

				case false:
					var posDeSel = _.filter(oListModel, {
						selected: false
					});
					posDeSel.forEach(function (n) {
						n.stateInputBolla = "None";
						n.qbolla = "";
						_.remove(this.cartSessionModel, {
							order: n.order,
							position: n.position
						});
					}.bind(this));
					this._setSessionStorageItem('cartData', this.cartSessionModel);
					this.visibleCartButton();
					this.listModel.refresh();
					var posEmpty = _.filter(oListModel, {
						selected: true,
						qbolla: ""
					});
					if (posEmpty.length) {
						var lastPosEmpty = posEmpty[posEmpty.length - 1];;
						for (var i = 0; i < oListModel.length; i++) {
							if (oListModel[i].order === lastPosEmpty.order && oListModel[i].position === lastPosEmpty.position) {
								break;
							}
						}
						setTimeout(function () {
							var inputBolla = $("[id*='tablePos-" + i + "-inner']:input");
							inputBolla.focus();
						});
					}
					break;
			}
		},

		onInputBollaWrite: function (evt) {
			var input = evt.getSource();
			var value = input.getValue();
			if (isNaN(value) || value === "0") {
				value = value.substring(0, value.length - 1);
				input.setValue(value);
			} else if (value) {
				value = parseFloat(value);
			}
			var rowSelect = evt.getSource().getBindingContext("listModel").getObject();
			rowSelect.qbolla = value;
			rowSelect.stateInputBolla = this.checkItemsSelect(rowSelect);
			_.remove(this.cartSessionModel, {
				order: rowSelect.order,
				position: rowSelect.position
			});
			if (rowSelect.stateInputBolla === "None" && rowSelect.selected) {
				this.cartSessionModel.push(rowSelect);
				this._setSessionStorageItem('cartData', this.cartSessionModel);
			}
			this.visibleCartButton();
			this.listModel.refresh();
		},

		checkItemsSelect: function (rowSelect) {
			// funzione che controlla lo stato della quantità bolla e ritorna lo stato
			var stateQbolla = "None";
			var differenceQuantity = (parseFloat(rowSelect.orderquantityposition) - parseFloat(rowSelect.deliveryquantity));
			var partExcess = (differenceQuantity * parseFloat(rowSelect.totexcess)) + parseFloat(rowSelect.orderquantityposition);
			if (rowSelect.qbolla > partExcess || !rowSelect.qbolla) {
				stateQbolla = "Warning";
			} else {
				stateQbolla = "None";
			}
			return stateQbolla;
		},

		visibleCartButton: function () {
			// imposto la visibilità dei bottoni in base allo stato 
			var oListModel = this.listModel.getProperty("/listPosition");
			var isRowWarning = _.find(oListModel, {
				stateInputBolla: "Warning",
				selected: true
			});
			var isRowSelect = _.find(oListModel, {
				selected: true
			});
			var visibleCartButton = !this.cartSessionModel.length || isRowWarning || !oListModel.length ? false : true;
			var visibleEmptyCartButton = this.cartSessionModel.length || isRowSelect ? true : false;
			this.visibleModel.setProperty("/visibleCartButton", visibleCartButton);
			this.visibleModel.setProperty("/visibleEmptyCartButton", visibleEmptyCartButton);
		},

		onPressCart: function (evt) {
			// header e subheader filterModel
			var oFilterModel = this.filterModel.getData();
			var warehousesSelectDesc = _.filter(oFilterModel.warehouses, {
				deposit: oFilterModel.warehouse
			});
			var supplierSelectDesc = _.filter(oFilterModel.suppliers, {
				provider: oFilterModel.supplier
			});
			var filterData = {
				"dateRec": oFilterModel.dateRec,
				"dateDoc": oFilterModel.dateDoc,
				"bolla": oFilterModel.bolla,
				"supplier": oFilterModel.supplier,
				"suppliersDesc": supplierSelectDesc["0"].namesup,
				"warehouse": oFilterModel.warehouse,
				"warehousesDesc": warehousesSelectDesc["0"].decription,
				"order": oFilterModel.order,
				"position": oFilterModel.position,
				"material": oFilterModel.material,
				"dateA": oFilterModel.dateA,
				"dateDa": oFilterModel.dateDa
			}
			this._setSessionStorageItem('filterData', filterData);
			this.cartSessionModel = this._getSessionStorageItem('cartData');
			this._setSessionStorageItem('cartData', this.cartSessionModel);
			this.navToCart();
		},

		onPressEmptyCart: function (evt) {
			MessageBox.confirm(this._getLocalText("confirmRemoveCart"), {
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				onClose: _.bind(this._removeElementCart, this)
			});
		},

		_removeElementCart: function (evt) {
			if (evt === MessageBox.Action.NO) {
				return;
			}
			this._removeSessionStorageItem('cartData');
			this.cartSessionModel = this._getSessionStorageItem('cartData') ? this._getSessionStorageItem('cartData') : [];
			var oListModel = this.listModel.getProperty("/listPosition");
			var cartRowSelect = _.filter(oListModel, {
				selected: true
			});
			cartRowSelect.forEach(function (n) {
				n.selected = false;
				n.qbolla = "";
				n.stateInputBolla = "None"
			});
			this.listModel.setProperty("/listPosition", cartRowSelect);
			this.setSearch();
			this.visibleCartButton();
			this.listModel.refresh();
		},

		OnPressnavBack: function (evt) {
			var filtered = this.filterModel.getData();
			if (filtered.dateDoc | filtered.dateRec | filtered.bolla | filtered.warehouse | filtered.supplier != "" ) {
				MessageBox.confirm(this._getLocalText("confirmAnnulOrder"), {
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					onClose: _.bind(this._navBack, this)
				});
			} else {
				this._navBack();
			}
		},

		_navBack: function (evt) {
			if (evt === MessageBox.Action.NO) {
				return;
			}
			this._removeSessionStorageItem('filterData');
			this._removeSessionStorageItem('cartData');
			history.back(-1);
			this.closeDialogBusy();
		},

		navToCart: function () {
			var oRouter = UIComponent.getRouterFor(this);
			oRouter.navTo("cart");
			this.closeDialogBusy();
		}
	});
});