sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"icms/em/utils/Busy",
	'sap/ui/model/json/JSONModel'
], function (Controller, Busy, JSONModel) {
	"use strict";
	/* eslint-disable no-console */
	/* eslint-disable no-alert */
	/*globals _:false */
	/*eslint linebreak-style:0*/
	return Controller.extend("icms.em.controller.BaseController", {
		onInit: function () {
			this.uiModel = new sap.ui.model.json.JSONModel();
			this.getView().setModel(this.uiModel, "ui");
			this.userModel = new JSONModel();
			this.getView().setModel(this.userModel, "userModel");
		},

		_userRouteMatched: function () {
			var userData = this._getSessionStorageItem('loginInfo');
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			if (userData) {
				var decrypt = window.atob(userData.user);
				this.userModel.setProperty("/user", decrypt);
			} else {
				this._removeSessionStorageItem('loginInfo');
				oRouter.navTo("login");
			}
		},

		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		getModel: function (sName) {
			return this.getView().getModel(sName);
		},

		setModel: function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},
		/*******************************************/
		/* CUSTOM */

		_getById: function (id) {
			return this.byId(id) ? this.byId(id) : sap.ui.getCore().getElementById(id);
		},

		openDialogBusy: function (text) {
			Busy.show(text);
		},

		closeDialogBusy: function () {
			Busy.hide();
		},

		_getLocalText: function (text) {
			if (this.getModel("i18n")) {
				return this.getModel("i18n").getProperty(text);
			} else {
				return text;
			}
		},

		_showMessageError: function (err) {
			this.closeDialogBusy();
			var msgError = "";
			msgError = this._getMessageError(err);
			if (!msgError) {
				msgError = this._getLocalText("errorLoadingData");
			}
			sap.m.MessageBox.error(msgError);
		},

		_getMessageError: function (err) {
			var msgError = "";
			if (err.response) {
				if (err.response.body &&
					typeof (err.response.body) === "string" &&
					err.response.body.indexOf("message") >= 0 &&
					JSON.parse(err.response.body) &&
					JSON.parse(err.response.body).error &&
					JSON.parse(err.response.body).error.message &&
					JSON.parse(err.response.body).error.message.value) {
					msgError = JSON.parse(err.response.body).error.message.value;
				} else if (err.response.statusCode === 500) {
					msgError = err.response.statusText;
				} else if (err.response.statusCode === 504) {
					msgError = err.response.statusText;
				} else {
					msgError = this._getLocalText("errorCall");
				}
			} else if (err.responseText && typeof (err.responseText) === "string") {
				if (err.responseText.indexOf("errorMessage") >= 0 &&
					JSON.parse(err.responseText).errorMessage) {
					msgError = JSON.parse(err.responseText).errorMessage;
				} else if (err.responseText.indexOf("error") >= 0 &&
					JSON.parse(err.responseText).error) {
					msgError = JSON.parse(err.responseText).error;
				}
			} else {
				msgError = err.statusText;
			}
			return msgError;
		},
		/*******************************************/
		/* SESSION STORAGE*/
		// passare gli items fra apici 'dati'

		_getSessionStorageItem: function (items) {
			var selectItems = sessionStorage.getItem(items) ? sessionStorage.getItem(items) : "";
			var returnItems = "";
			if (selectItems) {
				returnItems = JSON.parse(selectItems);
			}
			return returnItems;
		},
		_setSessionStorageItem: function (items, object) {
			var stringObject = JSON.stringify(object);
			sessionStorage.setItem(items, stringObject);
		},

		_removeSessionStorageItem: function (items) {
			var selectItems = sessionStorage.getItem(items) ? sessionStorage.getItem(items) : "";
			if (selectItems) {
				sessionStorage.removeItem(items);
			}
		},
		/*******************************************/
		/* LOCAL STORAGE*/
		// passare gli items fra apici 'dati'

		_getLocalStorageItem: function (items) {
			var selectItems = localStorage.getItem(items) ? localStorage.getItem(items) : "";
			var returnItems = "";
			if (selectItems) {
				returnItems = JSON.parse(selectItems);
			}
			return returnItems;
		},

		_setLocalStorageItem: function (items, object) {
			var stringObject = JSON.stringify(object);
			localStorage.setItem(items, stringObject);
		},

		_removeLocalStorageItem: function (items) {
			var selectItems = localStorage.getItem(items) ? localStorage.getItem(items) : "";
			if (selectItems) {
				localStorage.removeItem(items);
			}
		}
	});
});