sap.ui.define([
	'icms/em/controller/BaseController',
	'sap/ui/model/json/JSONModel',
	'sap/ui/core/UIComponent',
	'icms/em/model/Formatter',
	'icms/em/model/Callmodel',
], function (BaseController, JSONModel, UIComponent, Formatter, CallModel) {
	"use strict";

	return BaseController.extend("icms.em.controller.Recdocument", {
		formatter: Formatter,
		onInit: function (evt) {
			BaseController.prototype.onInit.apply(this, arguments);
			this.getRouter().getRoute("recdocument").attachPatternMatched(this._recDocumentMatched, this);
			this.filterModel = new JSONModel();
			this.getView().setModel(this.filterModel, "filterModel");
			this.visibleModel = new JSONModel();
			this.getView().setModel(this.visibleModel, "visibleModel");
			this.documentsModel = new JSONModel();
			this.getView().setModel(this.documentsModel, "documentsModel");
		},

		_recDocumentMatched: function () {
			BaseController.prototype._userRouteMatched.apply(this, arguments);
			this.setheaderfilter();
		},

		setheaderfilter: function () {
			this.filterModel.setProperty("/warehouse", "");
			this.filterModel.setProperty("/warehouses", []);
			this.filterModel.setProperty("/dateRecDa", null);
			this.filterModel.setProperty("/dateRecA", null);
			this.filterModel.setProperty("/bolla", "");
			this.visibleModel.setProperty("/state", "None");
			this.filterModel.setProperty("/supplier", "");
			this.filterModel.setProperty("/suppliers", []);
			this.filterModel.setProperty("/material", "");
			this.filterModel.setProperty("/materials", []);
			this.filterModel.setProperty("/division", "");
			this.filterModel.setProperty("/divisions", []);
			this.visibleModel.setProperty("/visibleRows", 0);
			this.visibleModel.setProperty("/visibleTable", false);
			this.visibleModel.setProperty("/expandedRadioButton", false);
			this.visibleModel.setProperty("/numDocPos", true);
			this.visibleModel.setProperty("/numCodMat", false);
			this.visibleModel.setProperty("/expanndedHeader", true);
			this.getWarehouses();
			this.getDivision();
		},

		getWarehouses: function () {
			var fSuccess = function (result) {
				this.filterModel.setProperty("/warehouses", result);
				this.getView().setBusy(false);
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);

			this.getView().setBusy(true);
			CallModel.getWarehouseList().then(fSuccess, fError);
		},

		onWarehouseChange: function (evt) {
			var warehouseselect = evt.getSource().getSelectedKey();
			this.getSuppliers(warehouseselect);
		},

		getSuppliers: function (warehouseselect) {
			var fSuccess = function (result) {
				this.filterModel.setProperty("/suppliers", result);
				this.getView().setBusy(false);
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);
			CallModel.getSupplierList(warehouseselect).then(fSuccess, fError);
			this.getView().setBusy(true);
		},

		onSupplierChange: function (evt) {
			var supplierselect = evt.getSource().getSelectedKey();
			this.getMaterials(supplierselect)
		},

		getMaterials: function (supplierSelect) {
			var fSuccess = function (result) {
				this.filterModel.setProperty("/materials", result);
				this.getView().setBusy(false);
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);
			CallModel.getMaterialList(supplierSelect).then(fSuccess, fError);
			this.getView().setBusy(true);
		},

		getDivision: function () {
			var fSuccess = function (result) {
				var divisionToShow = _.uniqBy(result, "division");
				this.filterModel.setProperty("/divisions", divisionToShow);
				this.getView().setBusy(false);
				this.closeDialogBusy();
			}.bind(this);
			var fError = function (err) {
				var msgError = this._getMessageError(err) ? this._getMessageError(err) : this._getLocalText("errorLoadingData");
				MessageBox.error(this._getLocalText(msgError));
				this.getView().setBusy(false);
			}.bind(this);
			CallModel.getDivisionList().then(fSuccess, fError);
			this.getView().setBusy(true);
		},

		onSearchPress: function (evt) {
			this.openDialogBusy(this._getLocalText('loading'));
			var filterModel = this.filterModel.getData();
			var documentsData = this.getDocumentSessios();
			if (documentsData) {
				_.remove(documentsData, function (n) {
					return n.warehouse !== filterModel.warehouse;
				})

				if (filterModel.dateRecDa) {
					_.remove(documentsData, function (n) {
						return Formatter.formatDateSession(n.dateRec) < filterModel.dateRecDa;
					});
				}
				if (filterModel.dateRecA) {
					_.remove(documentsData, function (n) {
						return Formatter.formatDateSession(n.dateRec) > filterModel.dateRecA;
					});
				}
				if (filterModel.bolla) {
					_.remove(documentsData, function (n) {
						return n.bolla !== filterModel.bolla;
					});
				}
				if (filterModel.supplier) {
					_.remove(documentsData, function (n) {
						return n.supplier !== filterModel.supplier;
					});
				}
				if (filterModel.material) {
					documentsData.forEach(function (i) {
						_.remove(i.listCartModel, function (n) {
							return n.material !== filterModel.material;
						});
					});
				}
				if (filterModel.division) {
					documentsData.forEach(function (i) {
						_.remove(i.listCartModel, function (n) {
							return n.division !== filterModel.division;
						});
					});
				}
				var arrayList = [];
				documentsData.forEach(function (n) {
					n.listCartModel.forEach(function (i) {
						i.bolla = n.bolla,
							i.dateCreateDoc = Formatter.formatInputDate(n.dateCreateDoc),
							i.timeCreateDoc = Formatter.formatInputTime(n.dateCreateDoc),
							i.dateDoc = Formatter.formatInputDate(n.dateDoc),
							i.dateRec = Formatter.formatInputDate(n.dateRec),
							i.docMM = n.docMM,
							i.protocolNumber = n.protocolNumber,
							i.supplier = n.supplier,
							i.suppliersDesc = n.suppliersDesc,
							i.warehouse = n.warehouse,
							i.warehousesDesc = n.warehousesDesc
						i.userLogin = n.userLogin
						arrayList.push(i);
					});
				});
				this.documentsModel.setData(arrayList);
				this.visibleModel.setProperty("/visibleRows", arrayList.length);
				this.visibleModel.setProperty("/visibleTable", true);
				this.visibleModel.setProperty("/expandedRadioButton", false);
				this.visibleModel.setProperty("/expanndedHeader", false);
				this.onOrderForNumDocPos();
				this.closeDialogBusy();
			} else {
				documentsData = [];
				this.documentsModel.setData(documentsData);
				this.visibleModel.setProperty("/visibleRows", documentsData.length);
				this.visibleModel.setProperty("/visibleTable", true);
				this.visibleModel.setProperty("/expandedRadioButton", false);
				this.visibleModel.setProperty("/expanndedHeader", false);
				this.onOrderForNumDocPos();
				this.closeDialogBusy();
			}
		},

		onResetPress: function (evt) {
			this.setheaderfilter();
		},

		getDocumentSessios: function () {
			var documentsData = this._getLocalStorageItem('documentsData');
			return documentsData;
		},

		setInputState: function (evt) {
			var input = evt.getSource().getValue();
			if (input) {
				this.visibleModel.setProperty("/state", "Success");
			} else
				this.visibleModel.setProperty("/state", "None");
		},

		onOrderForNumDocPos: function (evt) {
			this.visibleModel.setProperty("/numDocPos", true);
			this.visibleModel.setProperty("/numCodMat", false);
			var documentsModel = this.documentsModel.getData();
			var documentsOrder = _.sortBy(documentsModel, ['docMM', 'position']);
			this.documentsModel.setData(documentsOrder);
		},

		onOrderNumCodMat: function (evt) {
			this.visibleModel.setProperty("/numDocPos", false);
			this.visibleModel.setProperty("/numCodMat", true);
			var documentsModel = this.documentsModel.getData();
			var documentsOrder = _.sortBy(documentsModel, ['material', 'docMM', 'position']);
			this.documentsModel.setData(documentsOrder);
		},

		navBack: function (evt) {
			this.closeDialogBusy();
			history.back(-1);
		}
	});
});