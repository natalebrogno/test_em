sap.ui.define([
	"icms/em/utils/Util",
	"icms/em/utils/Serializer"
], function (util, Serializer) {
	"use strict";

	return {
		UserEntity: {
			fromSapItems: function (results) {
				var data = [];
				if (results && results.d && results.d.results && results.d.results.length > 0) {
					_.each(results.d.results, function (sap) {
						var c = this.fromSap(sap);
						data.push(c);
					}.bind(this));
				}
				return data;
			},
			////////////////////////////////////////////////////////////////////////////////////
			fromSap: function (sap) {
				var p = {};
				p.user =  window.btoa(sap.Bname);
				p.password =  window.btoa(sap.Bcode);
				p.cid =  window.btoa(sap.ILog);
				return p;
			}
		},
		WarehouseEntity: {
			fromSapItems: function (results) {
				var data = [];
				if (results && results.d && results.d.results && results.d.results.length > 0) {
					_.each(results.d.results, function (sap) {
						var c = this.fromSap(sap);
						data.push(c);
					}.bind(this));
				}
				return data;
			},
			////////////////////////////////////////////////////////////////////////////////////
			fromSap: function (sap) {
				var p = {};
				p.cap = sap.Cap;
				p.city = sap.Citta;
				p.deposit = sap.Deposito;
				p.decription = sap.Descrizione;
				p.province = sap.Provincia;
				p.street = sap.Via;
				return p;
			}
		},
		SupplierEntity: {
			fromSapItems: function (results) {
				var data = [];
				if (results && results.d && results.d.results && results.d.results.length > 0) {
					_.each(results.d.results, function (sap) {
						var c = this.fromSap(sap);
						data.push(c);
					}.bind(this));
				}
				return data;
			},
			/////////////////////////////////////////////////////////////////////////////////////
			fromSap: function (sap) {
				var p = {};
				p.all = sap.All;
				p.deposit = sap.Deposito;
				p.provider = sap.Fornitore;
				p.namesup = sap.Name1;
				p.vatnuber = sap.Partitaiva;
				p.contractor = sap.Terzista;
				return p;
			}
		},
		OrdersEntity: {
			fromSapItems: function (results) {
				var data = [];
				if (results && results.d && results.d.results && results.d.results.length > 0) {
					_.each(results.d.results, function (sap) {
						var c = this.fromSap(sap);
						data.push(c);
					}.bind(this));
				}
				return data;
			},
			/////////////////////////////////////////////////////////////////////////////////////
			fromSap: function (sap) {
				var p = {};
				p.all = sap.All;
				p.codmatsuppliers = sap.CodMatFornitore;
				p.dateshipmetA = sap.DataSpedA;
				p.dateshipmetDa = sap.DataSpedDa;
				p.division = sap.Divisione;
				p.outcome = sap.Esito;
				p.provider = sap.Fornitore;
				p.matirial = sap.Materiale
				p.nuberDa = sap.NumeroOda
				p.ordernuber = sap.Ordine
				p.position = sap.Posizione
				p.contractor = sap.Terzista;
				return p;
			}
		},
		MaterialsEntity: {
			fromSapItems: function (results) {
				var data = [];
				if (results && results.d && results.d.results && results.d.results.length > 0) {
					_.each(results.d.results, function (sap) {
						var c = this.fromSap(sap);
						data.push(c);
					}.bind(this));
				}
				return data;
			},
			/////////////////////////////////////////////////////////////////////////////////////
			fromSap: function (sap) {
				var p = {};
				p.all = sap.All;
				p.decription = sap.Descrizione;
				p.provider = sap.Fornitore;
				p.contractor = sap.Terzista;
				p.outcome = sap.Esito;
				p.material = sap.Materiale;
				return p;
			}
		},
		ListEntity: {
			fromSapItems: function (results) {
				var data = [];
				if (results.d.ListaOdaOut.results && results.d.ListaOdaOut.results.length > 0) {
					_.each(results.d.ListaOdaOut.results, function (sap) {
						var c = this.fromSap(sap);
						data.push(c);
					}.bind(this));
				}
				return data;
			},
			/////////////////////////////////////////////////////////////////////////////////////
			fromSap: function (sap) {
				var p = {};
				p.order = sap.Ordine;
				p.position = sap.Posizione;
				p.material = sap.Materiale;
				p.descmaterial = sap.DescrMateriale;
				p.codmatsupp = sap.CodiceMatPressoForn;
				p.descprovider = sap.DescrFornitore;
				p.deliverydate = util.convertDateFromSap(sap.DataConsegna);
				p.deliverydateFormat = util.convertDateFormat(sap.DataConsegna);
				p.orderquantityposition = sap.QuantOrdineUmo.trim();
				p.part = sap.Umo;
				p.deliveryquantity = sap.QuantConsegnata.trim();
				p.totexcess = sap.TollEccesso;
				p.totdefect = sap.TollDifetto;
				p.totunlimited = sap.TollIllimitata;
				p.division = sap.Divisione;
				p.contractor = sap.Terzista;
				p.dumo = sap.DUmo;
				p.selected = false;
				p.stateInputBolla = "None";
				p.qbolla = "";
				return p;
			}
		}
	};
});