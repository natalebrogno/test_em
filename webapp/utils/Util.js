sap.ui.define([
	"icms/em/utils/Util",
	"sap/ui/core/format/DateFormat"
], function (util, dateFormat) {
	"use strict";

	return {
		convertDateFromSap: function (date) {
			var dd = date.substring(6, 8);
			var MM = date.substring(4, 6);
			var yyyy = date.substring(0, 4);
			var convertedDate = dd + "/" + MM + "/" + yyyy;
			return convertedDate;
		},

		convertDateFormat: function (date) {
			var dd = date.substring(6, 8);
			var MM = date.substring(4, 6);
			var mm = MM - 1;
			var yyyy = date.substring(0, 4);
			var dateFormat = new Date(yyyy, mm, dd);
			return dateFormat;
		},
		reverseDate: function (date) {
			var dd = date.substring(8, 10);
			var MM = date.substring(5, 7);
			var yyyy= date.substring(0, 4);
			var dateFormat = dd + "/" + MM + "/" + yyyy;
			return dateFormat;
		},
	}
});