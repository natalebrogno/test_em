sap.ui.define([
	"icms/em/Component",
	"icms/em/utils/Util"
], function (Component, util) {
	"use strict";

	return {

		// // SERVIZIO ODATA		
		getServiceUser: function () {
			var _serviceUrl = Component.getMetadata().getManifestEntry("sap.app").dataSources["userService"].uri;
			return new sap.ui.model.odata.ODataModel(_serviceUrl, true);
		},
		getUser: function (success, error) {
			$.ajax({
				url: 'webapp/mockdata/Users.json',
				type: 'GET',
				dataType: 'json',
				success: success,
				error: error,
				async: true,
			});
		},
		getWarehouse: function (success, error) {
			$.ajax({
				url: 'webapp/mockdata/DepositiSet.json',
				type: 'GET',
				dataType: 'json',
				success: success,
				error: error,
				async: true,
			});
		},
		getSupplier: function (success, error) {
			$.ajax({
				url: 'webapp/mockdata/FornitoriSet.json',
				type: 'GET',
				dataType: 'json',
				success: success,
				error: error,
				async: true,
			});
		},
		getOrdersService: function (success, error) {
			$.ajax({
				url: 'webapp/mockdata/OrdiniSet.json',
				type: 'GET',
				dataType: 'json',
				success: success,
				error: error,
				async: true,
			});
		},
		getMaterialsService: function (success, error) {
			$.ajax({
				url: 'webapp/mockdata/MaterialiSet.json',
				type: 'GET',
				dataType: 'json',
				success: success,
				error: error,
				async: true,
			});
		},
		getListService: function (success, error) {
			$.ajax({
				url: 'webapp/mockdata/List.json',
				type: 'GET',
				dataType: 'json',
				success: success,
				error: error,
				async: true,
			});
		}
	};
});