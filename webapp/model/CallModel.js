sap.ui.define([
	"icms/em/model/ODataRequest",
	"icms/em/utils/Serializer",
], function (oDataRequest, serializer) {
	"use strict";

	return {
		getUserList: function (user, password) {
			var _defer = Q.defer();
			var fSuccess = function (result) {
				var data = serializer.UserEntity.fromSapItems(result);
				var dataArrayLog = _.filter(data, {
					user: user,
					password: password,
				});
				if (dataArrayLog.length) {
					var dataLog = dataArrayLog[0];
					if (dataLog.user && dataLog.password) {
						_defer.resolve(dataLog);
					}
				} else {
					_defer.reject(data);
				}
			};
			var fError = function (err) {
				error(err);
			};
			oDataRequest.getUser(fSuccess, fError);
			return _defer.promise;
		},
		getWarehouseList: function () {
			var _defer = Q.defer();
			var fSuccess = function (result) {
				var data = serializer.WarehouseEntity.fromSapItems(result);
				_defer.resolve(data);
				// _defer.resolve(result)
			};
			var fError = function (err) {
				_defer.reject(err);
			};
			oDataRequest.getWarehouse(fSuccess, fError);
			return _defer.promise;
		},
		getSupplierList: function (whselect) {
			var _defer = Q.defer();
			var fSuccess = function (result) {
				var data = serializer.SupplierEntity.fromSapItems(result);
				var supllierFiltered = _.filter(data, {
					deposit: whselect
				});
				_defer.resolve(supllierFiltered);
				// _defer.resolve(result)
			};
			var fError = function (err) {
				_defer.reject(err);
			};
			oDataRequest.getSupplier(fSuccess, fError);
			return _defer.promise;
		},
		getOrdersList: function (supplierSelect) {
			var _defer = Q.defer();
			var fSuccess = function (result) {
				var data = serializer.OrdersEntity.fromSapItems(result);
				var supllierFiltered = _.filter(data, {
					provider: supplierSelect
				});
				_defer.resolve(supllierFiltered);
				// _defer.resolve(result)
			};
			var fError = function (err) {
				_defer.reject(err);
			};
			oDataRequest.getOrdersService(fSuccess, fError);
			return _defer.promise;
		},
		getPositionList: function (order) {
			var _defer = Q.defer();
			var fSuccess = function (result) {
				var data = serializer.OrdersEntity.fromSapItems(result);
				// Ciclo ogni ordine selezionotoe di ogni ordine filtro in base alla posizione
				var allFilteredOrders = [];
				order.forEach(function (n) {
					var filteredOrders = _.filter(data, {
						ordernuber: n
					});
					allFilteredOrders = allFilteredOrders.concat(filteredOrders);
				});
				_defer.resolve(allFilteredOrders);
			};
			var fError = function (err) {
				_defer.reject(err);
			};
			oDataRequest.getOrdersService(fSuccess, fError);
			return _defer.promise;
		},
		getMaterialList: function (supplierSelect) {
			var _defer = Q.defer();
			var fSuccess = function (result) {
				var data = serializer.MaterialsEntity.fromSapItems(result);
				var supllierFiltered = _.filter(data, {
					provider: supplierSelect
				});
				_defer.resolve(supllierFiltered);
				// _defer.resolve(result)
			};
			var fError = function (err) {
				_defer.reject(err);
			};
			oDataRequest.getMaterialsService(fSuccess, fError);
			return _defer.promise;
		},
		getListData: function (SearchFilter) {
			var _defer = Q.defer();
			var fSuccess = function (result) {
				var data = serializer.ListEntity.fromSapItems(result);
				var arrayToSend = data;
				var arrayDataDa = [];
				var dataIsFiltered = false;
				if (SearchFilter) {
					var allFilteredData = [];
					var order = SearchFilter.order;
					var position = SearchFilter.position;
					var material = SearchFilter.material;
					var dateDa = SearchFilter.dateDa;
					var dateA = SearchFilter.dateA;

					if (order.length) {
						dataIsFiltered = true;
						order.forEach(function (n) {
							var filteredDataOrder = _.filter(data, {
								order: n
							});
							allFilteredData = allFilteredData.concat(filteredDataOrder);
						});
					}
					if (position.length) {
						dataIsFiltered = true;
						var arrayToFilter = allFilteredData.length ? allFilteredData : data;
						var allOrderFilter = [];
						position.forEach(function (n) {
							var orderFilter = _.filter(arrayToFilter, {
								position: n
							});
							allOrderFilter = allOrderFilter.concat(orderFilter);
						});
						allFilteredData = allOrderFilter;
					}
					if (material.length) {
						dataIsFiltered = true;
						var arrayToFilter = allFilteredData.length ? allFilteredData : data;
						var allMaterialFilter = [];
						material.forEach(function (n) {
							var materialFilter = _.filter(arrayToFilter, {
								material: n
							});
							allMaterialFilter = allMaterialFilter.concat(materialFilter);
						});
						allFilteredData = allMaterialFilter;
					}
					if (dateDa) {
						dataIsFiltered = true;
						var arrayToFilter = allFilteredData.length ? allFilteredData : data;
						var filteredDataDa = _.filter(arrayToFilter, function (n) {
							return n.deliverydateFormat >= dateDa
						});
						arrayDataDa = arrayDataDa.concat(filteredDataDa);
						allFilteredData = arrayDataDa;
					}
					if (dateA) {
						dataIsFiltered = true;
						var arrayToFilter = allFilteredData.length ? allFilteredData : data;
						var arrayData = [];
						var filteredDataA = _.filter(dateDa ? arrayDataDa : arrayToFilter, function (n) {
							return n.deliverydateFormat <= dateA
						});
						arrayData = arrayData.concat(filteredDataA);
						allFilteredData = arrayData;
					}
					arrayToSend = dataIsFiltered ? allFilteredData : data;
				}
				_defer.resolve(arrayToSend);
			};
			var fError = function (err) {
				_defer.reject(err);
			};
			oDataRequest.getListService(fSuccess, fError);
			return _defer.promise;
		},
		getDivisionList: function () {
			var _defer = Q.defer();
			var fSuccess = function (result) {
				var data = serializer.ListEntity.fromSapItems(result);
				var divisionSelect = [];
				data.forEach(function (n) {
					var filterDivision = {
						"division": n.division
					}
					divisionSelect = divisionSelect.concat(filterDivision);
				});
				_defer.resolve(divisionSelect);
				// _defer.resolve(result)
			};
			var fError = function (err) {
				_defer.reject(err);
			};
			oDataRequest.getListService(fSuccess, fError);
			return _defer.promise;
		}
	};
});