sap.ui.define([], function () {
    "use strict";

    return {
        // Creo un modello in cui costruisco le tile 
        getTiles: function () {
            var tiles = [{
                    "icon": "sap-icon://product",
                    "title": "goodsEntry",
                    "infoState": "Success",
                    "url": "goodsentry"
                },{
                    "icon": "sap-icon://create",
                    "title": "registeredDocuments",
                    "infoState": "Success",
                    "url": "recdocument"
                }
            ];
            return tiles;
        }
    };
});