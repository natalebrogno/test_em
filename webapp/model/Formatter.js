sap.ui.define([
	"icms/em/utils/Util"
], function (util) {
	"use strict";
	return {
		formatQuantity: function (quantity, unit) {
			if (unit === "PZ") {
				var formatedQ = parseInt(quantity) + " " + unit;
			}
			return formatedQ;
		},
		formatDescMaterial: function (material, descmaterial) {
			var formatMaterial = material + "\n" + descmaterial;
			return formatMaterial;
		},
		formatInputDate: function (date) {
			//2019-03-12T23:00:00.000Z
			if (date) {
				var dateFormat = date.substring(0, 10)
				return util.reverseDate(dateFormat);
			} else {
				return date;
			}
		},
		formatDateSession: function (date) {
			//2019-03-12T23:00:00.000Z
			if (date) {
				var dateFormat = date.substring(0, 10);
				var newDate = new Date(dateFormat);
				return newDate;
			}
		},
		formatInputTime: function (date) {
			var dateFormat = new Date(date);
			var hours = dateFormat.getHours()<10 ? "0" + dateFormat.getHours() : dateFormat.getHours();;
			var minutes = dateFormat.getMinutes()<10 ? "0" + dateFormat.getMinutes() : dateFormat.getMinutes();
			var timeReturn = hours + ":" + minutes;
			return timeReturn;
		},
		formatDateLaunchpad: function (date) {
			var dateString = date.toString();
			var gg = dateString.substring(8, 10);
			var mm = dateString.substring(4, 7);
			var yyyy = dateString.substring(11, 15);
			var dateFormat = gg + " " + mm + " " + yyyy;
			return dateFormat;
		},
		formatDelQuantity: function(quantity){
			var quantityDecimal = parseFloat(quantity).toFixed(2);
			return quantityDecimal;
		}
	};
});