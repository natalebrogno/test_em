sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/model/json/JSONModel",
], function (UIComponent, JSONModel) {
	"use strict";

	return UIComponent.extend("icms.em.Component", {

		metadata: {
			manifest: "json"
		},

		init: function () {

			// call the init function of the parent
			UIComponent.prototype.init.apply(this, arguments);

			// create the views based on the url/hash
			this.getRouter().initialize();

			// modello che mi permette di avere informazioni 
			// sul dispositivo richiamabile in ogni controller
			var deviceModel = new JSONModel();
			this.setModel(deviceModel, "deviceModel");

			var device = sap.ui.Device;
			deviceModel.setData(device);

		},

		exit: function () {

		},
		createContent: function () {
			// create root view
			var oView = sap.ui.view({
				id: "mainApp",
				viewName: "icms.em.view.App",
				type: "XML",
				viewData: {
					component: this
				}
			});

			return oView;
		}
		
	});

});
